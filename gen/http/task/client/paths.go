// Code generated by goa v3.12.3, DO NOT EDIT.
//
// HTTP request path constructors for the task service.
//
// Command:
// $ goa gen gitlab.eclipse.org/eclipse/xfsc/tsa/task/design

package client

import (
	"fmt"
)

// CreateTaskPath returns the URL path to the task service Create HTTP endpoint.
func CreateTaskPath(taskName string) string {
	return fmt.Sprintf("/v1/task/%v", taskName)
}

// TaskResultTaskPath returns the URL path to the task service TaskResult HTTP endpoint.
func TaskResultTaskPath(taskID string) string {
	return fmt.Sprintf("/v1/taskResult/%v", taskID)
}
