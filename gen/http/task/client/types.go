// Code generated by goa v3.12.3, DO NOT EDIT.
//
// task HTTP client types
//
// Command:
// $ goa gen gitlab.eclipse.org/eclipse/xfsc/tsa/task/design

package client

import (
	task "gitlab.eclipse.org/eclipse/xfsc/tsa/task/gen/task"
	goa "goa.design/goa/v3/pkg"
)

// CreateResponseBody is the type of the "task" service "Create" endpoint HTTP
// response body.
type CreateResponseBody struct {
	// Unique task identifier.
	TaskID *string `form:"taskID,omitempty" json:"taskID,omitempty" xml:"taskID,omitempty"`
}

// NewCreateTaskResultOK builds a "task" service "Create" endpoint result from
// a HTTP "OK" response.
func NewCreateTaskResultOK(body *CreateResponseBody) *task.CreateTaskResult {
	v := &task.CreateTaskResult{
		TaskID: *body.TaskID,
	}

	return v
}

// ValidateCreateResponseBody runs the validations defined on CreateResponseBody
func ValidateCreateResponseBody(body *CreateResponseBody) (err error) {
	if body.TaskID == nil {
		err = goa.MergeErrors(err, goa.MissingFieldError("taskID", "body"))
	}
	return
}
